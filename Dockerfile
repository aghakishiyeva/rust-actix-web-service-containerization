FROM rust:1.76 as builder
WORKDIR /usr/src/actix_web_app
COPY . .
RUN cargo build --release
RUN ls /usr/src/actix_web_app/target/release/

FROM debian:latest
COPY --from=builder /usr/src/actix_web_app/target/release/miniproject4 /usr/local/bin/miniproject4
EXPOSE 8080
CMD ["miniproject4"]

# Containerizing a Rust Actix Web Service with Docker

This repository contains a simple **Rust Actix Web Service** that has been containerized using **Docker**.

<div align="center">
  <img src="https://miro.medium.com/v2/resize:fit:1400/format:webp/1*rye3eqP0k0kSX_jSxURcmg.png">
</div>

## Project Overview
The web service consists of a single endpoint that responds with a greeting message.

## Project Structure
- **main.rs**: Contains the main code for the Actix web service.
- **Dockerfile**: Defines the Docker image build process.
- **Cargo.toml**: Specifies project metadata and dependencies.

## Getting Started
1. Clone this repository to your local machine:

   ```bash
   git clone https://gitlab.com/aghakishiyeva/rust-actix-web-service-containerization.git
   ```

2. Ensure you have Rust installed:

    ```bash
    rustc --version
    ```

3. Ensure you have Docker installed:

    ```bash
    docker --version
    ```

4. Build and run the project locally using Cargo:

    ```bash
    cargo run
    ```

5. Build the Docker image using the provided Dockerfile:

    ```bash
    docker build -t rust-actix-web-service .
    ```

6. Run the Docker container locally:

    ```bash
    docker run -d -p 8080:8080 rust-actix-web-service
    ```

6. Make sure Docker is running:

    ```bash
    docker ps
    ```

## Accessing the Web Service

Once the Docker container is running, you can access the web service by navigating to `http://localhost:8080` in your web browser.

